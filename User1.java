package com.krish;

/**
 * This is implimentation of a class User1 It represents the users in a User
 * 
 * @author vchukka
 *
 */
public class User1 {
	String name;
	int age;
	String profession;
	String phone_no;
	String hobbie;

	/**
	 * To create a method with arguments and calling all the instance variables
	 * 
	 * @param name
	 * @param age
	 * @param profession
	 * @param phone_no
	 * @param hobbie
	 */
	// parameterized constructor
	User1(String name, int age, String profession, String phone_no, String hobbie) {
		this.name = name; // representation of name
		this.age = age;// representation of age
		this.profession = profession;// representation of profession
		this.phone_no = phone_no;// representation of phone_no
		this.hobbie = hobbie;// representation of hobbie

	}

	/**
	 * This is toString method of an user1 class object in human redable form
	 */
	@Override
	public String toString() {
		String stringreturn = "";
		stringreturn += "name : " + this.name + "\n";
		stringreturn += "age :" + this.age + "\n";
		stringreturn += "profession:" + this.profession + "\n";
		stringreturn += "phone_no" + this.phone_no + "\n";
		stringreturn += "hobbie :" + this.hobbie + "\n";
		return stringreturn;

	}

}
