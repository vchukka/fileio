package com.krish;

import java.io.FileWriter;
import java.io.IOException;

/**
 * This is the main class to test all the various objects on here
 * @author vchukka
 *
 */
public class Test {
	public static void main(String[]args) {
		User1 obj = new User1("krishna", 23, "java developer", "8790312879","playing");
		User1 obj1 = new User1("rekha", 22, "java developer", "9956341209","crying");
		User1 obj2 = new User1("priya", 20, "java developer", "8948594840","sleeping");
		
	/**
	 * This is FileWriter it contains  a path location the created objects shown in the text file 
	 * with the help of this method in human readable form
	 */
	try (FileWriter file = new FileWriter("C:\\Users\\vchukka\\eclipse-workspace 01\\FileIO.txt")) {
		file.write(obj.name);//To show the name of first object in the above  file 
        file.write(obj.phone_no);//To show the phone_no of first object in the above file  
        file.write(obj1.hobbie);//To show the hobbie of second object in the above  file  
        file.write(obj1.name);//To show the name of second object in the above file 
        file.write(obj2.age);//To show the age of third object in the above file  
        file.write(obj2.name);//To show the name of third object in the above file 
        file.write(obj.hobbie);//To show the hobbie of first object in the above file  
        file.write(obj2.hobbie);//To show the hobbie of the third object in the above file  
    } catch (IOException e) {
        e.printStackTrace();
    }

    System.out.print(obj);
    System.out.println(obj1);
    System.out.println(obj2);

}
	

}
