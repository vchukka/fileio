package com.krish;
import java.io.File;

import java.io.IOException;
/**
 * This is CreateFile class it  defines how to create a text file in the respected given location
 * @author vchukka
 *
 */
public class CreateFile {
	public static void main(String[]args) {
		File f = new File("C:\\\\Users\\\\vchukka\\\\eclipse-workspace 01\\\\FileIO.txt");
		boolean result;
		try {
			result = f.createNewFile();
			if(result)
			{
				System.out.println("file created"+f.getCanonicalPath());
				
			}
			else {
				System.out.println("file already exist at location:"+f.getCanonicalPath());
				
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		
	}

}
